# Aconex Vehicle Survey Coding Challenge

## Overview
This is a demo application for the Aconex Vehicle Survey Coding Challenge.
This is coded in JavaScript using the [AngularJS][angularjs] framework to demonstrate how a visual representation is the best means
to meet all the requirements outlined in the challenge.
The application is a Single Page Application (SPA) and all code execution is done client-side in the browser.

## Prerequisites

### Modern Browser
This project will run on any modern browser, on either desktop and or mobile device.


Live Demo:
https://mnpvehiclesurvey.firebaseapp.com/

[angularjs]: https://angularjs.org/