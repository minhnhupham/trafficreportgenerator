// GLOBAL DEFINES/VARIABLES
var DEBUG_LEVEL = 1;

/****************************************************/
/* Description: Check if a variable is an array type*/
/* INPUTS: pia_myArray                              */
/* OUTPUT: Returns True if input is if type array   */
/****************************************************/
function isArray(pia_myArray) {
  return pia_myArray.constructor.toString().indexOf("Array") > -1;
}


/*****************************************************************************/
/* Description: 
/*     Logs messages to console.log base on global Debug level configuration or user passed in level */
/* INPUTS: pis_msg                                                            */
/* OUTPUT:                                                                   */
/*****************************************************************************/
function debugLog(pis_msg, dbgLevel) {
  // var ln_dbgLevel = typeof dbgLevel !== undefined ? dbgLevel : DEBUG_LEVEL;
  if (dbgLevel === undefined) {
    dbgLevel = DEBUG_LEVEL;
  }

  if(DEBUG_LEVEL > 0) {
  	if(dbgLevel <= DEBUG_LEVEL ) {
      console.log(pis_msg);
  	}    
  }
}

