// Vehicle Sensor Counter
var vcounter = {};

/*** GLOBAL DEFINES ***/
vcounter.DEF_WHEELBASE = 2.5;
vcounter.DEF_CF_MPS_TO_KPH = 36 / 10;  // Conversion factor for meters per second (m/s) into kilometers per hour (k/h)
vcounter.DEF_MS_IN_SECOND = 1000;
vcounter.DEF_MS_IN_MINUTE = vcounter.DEF_MS_IN_SECOND * 60;
vcounter.DEF_MS_IN_HOUR = vcounter.DEF_MS_IN_MINUTE * 60;
vcounter.DEF_MS_IN_DAY = vcounter.DEF_MS_IN_HOUR * 24;    // 86400000  milliseconds


/**************************************************************
Description: Vehicle Object Constructor
INPUTS/ATTRIBS:
   frontAxelTime => vehicle front axel tripped sensor
   rearAxelTime => vehicle back axel tripped sensor
   heading => direction the vehicle was heading
   day => relative day the sensor event tripped
   wheelBase => [optional ]wheel base of the vehicle

METHODS:
   + get_estSpeed() => Returns the estimated vehicle speed in meters per second (m/s)
      If no rear axel time metric is found return 0

   + get_frontAxelRealTime() => Returns frontAxelTime into real Data object offset by EPOCH

   + get_rearAxelRealTime() => Returns rearAxelTime into real Data object offset by EPOCH
**************************************************************/
vcounter.o_vehicle = function(pin_frontAxelTime, pin_rearAxelTime, pis_heading, pin_day, pin_wheelBase) {
   this.frontAxelTime = typeof pin_frontAxelTime !== undefined ? pin_frontAxelTime : 0;
   this.rearAxelTime = typeof pin_rearAxelTime !== undefined ? pin_rearAxelTime : 0;

   this.heading = "unknown";
   if (pis_heading !== undefined) {
      this.heading = pis_heading;
   }

   this.day = 0; // Default value to 0 if no day was set
   if (pin_day !== undefined) {
      this.day = pin_day;
   }

   this.wheelBase = vcounter.DEF_WHEELBASE;  // Default to value of global DEF_WHEELBASE if not passed in
   if (pin_wheelBase !== undefined) {
      this.wheelBase = pin_wheelBase;
   }

   this.get_estSpeed = function(lb_kph) {
      var speed = 0;
      var timeDuration = 0;
      if (this.rearAxelTime !== 0) {
         timeDuration = this.rearAxelTime - this.frontAxelTime;
         /* Calculate speed in standard SI unit of meters per second
            v = d / t
         */
         speed = this.wheelBase / (timeDuration / 1000);  // meters per second
      }
      if(lb_kph !== undefined) {
         return speed * vcounter.DEF_CF_MPS_TO_KPH;
      }
      else {
         return speed;
      }
   }

   this.get_frontAxelRealTime = function () {
      var timeFactor = this.day * vcounter.DEF_MS_IN_DAY;
      ld_realTime = new Date(this.frontAxelTime + timeFactor);
      return ld_realTime;
   }

   this.get_rearAxelRealTime = function () {
      var timeFactor = this.day * vcounter.DEF_MS_IN_DAY;
      ld_realTime = new Date(this.rearAxelTime + timeFactor);
      return ld_realTime;
   }

}  //END: o_vehicle


/**************************************************************
Description: Vehicle Group Object Constructor
INPUTS:
   pis_groupID  => Group ID

ATTRIBS:
   groupID => Group ID
   vehicles => array of o_vehicle

METHODS:
   get_vehiclesCount => Returns a count of all objects in vehicles[]
   get_vehiclesAvgSpeed => Average speed of all o_vehicle in vehicles[]
   get_vehiclesAvgSepDistance => Average vehicle seperation distance in meters
**************************************************************/
vcounter.o_vehicleGroup = function(pis_groupID) {
   this.groupID = "";
   if (pis_groupID !== undefined) {
      this.groupID = pis_groupID;
   }

   this.vehicles = [];

   this.get_vehiclesCount = function() {
      return this.vehicles.length;
   }

   this.get_vehiclesAvgSpeed = function(lb_kph) {
      var ln_speedAgg = 0;
      var ln_AvgSpeed = 0;
      for (var i=0; i<this.vehicles.length; i++) {
         ln_speedAgg += this.vehicles[i].get_estSpeed();
      }

      ln_AvgSpeed = ln_speedAgg / this.vehicles.length; // Averaged speed per group

      if ( isNaN(ln_AvgSpeed) ) {
         ln_AvgSpeed = 0;
      }

      if(lb_kph !== undefined) {
         return ln_AvgSpeed * vcounter.DEF_CF_MPS_TO_KPH;
      }
      else {
         return ln_AvgSpeed;
      }
   }

   this.get_vehiclesAvgSepDistance = function () {
      var ln_avgDist = 0;
      if (this.vehicles.length > 1) {
         var ln_totalDist = 0;
         for (var i=1; i<this.vehicles.length; i++) { // Find seperation distance per vehicle, starting at second vehicle
            var vehicles = this.vehicles;
            var deltaT = (vehicles[i].get_frontAxelRealTime() - vehicles[i - 1].get_rearAxelRealTime()) / vcounter.DEF_MS_IN_SECOND;
            ln_totalDist += deltaT * vehicles[i - 1].get_estSpeed();
         }
         ln_avgDist = ln_totalDist / (this.vehicles.length - 1);
      }
      return ln_avgDist;
   }
}  // END: o_vehicleGroup


/**************************************************************
Description: Code to process raw sensor data
INPUTS:
   pis_sensor_data => Input Sensor data
OUTPUT:
   la_parsedData =>  Array of o_vehicle objects
**************************************************************/
vcounter.parseSensorData = function(pis_sensorData) {
   var la_sensorData = pis_sensorData.split("\n");
   var la_readBuffer = ['EMPTY','EMPTY','EMPTY'];
   var rgx_validData = /^(A|B)([0-9].+)/;
   var rgx_timeData = /^[A|B]([0-9].+)/;
   var la_parsedData = [];
   var ln_day = 0;


   try {
      // Check input data is an array type
      if(!isArray(la_sensorData)) {
         throw "Input sensor data is not valid format";
      }

      debugLog("Sensor Events #: " + la_sensorData.length, 3);

      for (var i = 0; i < la_sensorData.length; i++) {
         var heading = "";

         // CHECK to see data is valid format
         if (la_sensorData[i] === null || la_sensorData[i] === "" ) {  // skip over blank lines
            debugLog("Blank line found at #: " + (i + 1), 3);
         }
         else if(rgx_validData.test(la_sensorData[i])) {  // Valid data found do processing here
            // Push data into buffer
            la_readBuffer.push(la_sensorData[i]);  // There should be 4 items in the buffer array

            // Scan buffer for pattern match
            debugLog(la_readBuffer, 3);
            var ls_buffer = la_readBuffer[0][0] + la_readBuffer[1][0] + la_readBuffer[2][0] + la_readBuffer[3][0];

            if (ls_buffer === "AAAA" || ls_buffer === "AAAB") {  // For pattern "AAAA" or "AAAB", northbound vehicle
               heading = 'north';
               var ln_frontAxelTime = Number(rgx_timeData.exec(la_readBuffer[0])[1]);
               var ln_rearAxelTime = Number(rgx_timeData.exec(la_readBuffer[1])[1]);

               // Check the rearAxelTime with previous item in la_parsedData
               // IF ln_rearAxelTime < la_parsedData[la_parsedData.length - 1].rearAxelTime = day has changed, increment day by 1
               if (la_parsedData.length > 0) { // check there is at least 1 item in la_parsedData
                  if (ln_rearAxelTime < la_parsedData[la_parsedData.length - 1].rearAxelTime) {
                     ln_day++;
                  }
               }

               var lo_vehicle = new vcounter.o_vehicle(ln_frontAxelTime, ln_rearAxelTime, heading, ln_day);
               la_parsedData.push(lo_vehicle);


               // Remove match from readBuffer
               la_readBuffer[0] = 'EMPTY';
               la_readBuffer[1] = 'EMPTY';
            }
            else if (ls_buffer === "ABAB") {  // For pattern 'ABAB', southbound vehicle
               debugLog("Found pattern ABAB", 3);
               heading = 'south';
               var ln_frontAxelTime = Number(rgx_timeData.exec(la_readBuffer[1])[1]);
               var ln_rearAxelTime = Number(rgx_timeData.exec(la_readBuffer[3])[1]);

               // Check the rearAxelTime with previous item in la_parsedData
               // IF ln_rearAxelTime < la_parsedData[la_parsedData.length - 1].rearAxelTime = day has changed, increment day by 1
               if (la_parsedData.length > 0) { // check there is at least 1 item in la_parsedData
                  if (ln_rearAxelTime < la_parsedData[la_parsedData.length - 1].rearAxelTime) {
                     ln_day++;
                  }
               }

               var lo_vehicle = new vcounter.o_vehicle(ln_frontAxelTime, ln_rearAxelTime, heading, ln_day);
               la_parsedData.push(lo_vehicle);

               // Remove match from readBuffer
               la_readBuffer[0] = 'EMPTY';
               la_readBuffer[1] = 'EMPTY';
               la_readBuffer[2] = 'EMPTY';
               la_readBuffer[3] = 'EMPTY';
            }
            else {
               debugLog("No match in read buffer.", 3);
            }

            // shift buffer one forward
            la_readBuffer.shift();
         }
         else {  // Sensor data contains invalid input line
            throw 'Invalid sensor data found at line #: ' + (i + 1);
         }
      }

      // Check readBuffer for left over data
      var ls_buffer = la_readBuffer[0][0] + la_readBuffer[1][0] + la_readBuffer[2][0];

      if (ls_buffer === 'EAA' && la_readBuffer[3] === undefined) {  // For pattern 'EAA', northbound vehicle
         var ln_frontAxelTime = Number(rgx_timeData.exec(la_readBuffer[1])[1]);
         var ln_rearAxelTime = Number(rgx_timeData.exec(la_readBuffer[2])[1]);

         // Check the rearAxelTime with previous item in la_parsedData
         // IF ln_rearAxelTime < la_parsedData[la_parsedData.length - 1].rearAxelTime = day has changed, increment day by 1
         if (la_parsedData.length > 0) { // check there is at least 1 item in la_parsedData
            if (ln_rearAxelTime < la_parsedData[la_parsedData.length - 1].rearAxelTime) {
               ln_day++;
            }
         }

         var lo_vehicle = new vcounter.o_vehicle(ln_frontAxelTime, ln_rearAxelTime, 'north', ln_day);
         la_parsedData.push(lo_vehicle);
      }
      else if (ls_buffer === 'EEE' && la_readBuffer[3] === undefined) {  // For pattern 'EEE'
         debugLog("Found pattern EEE", 3);
         debugLog("Finished processing data file.");
      }
      else {
         throw 'Data file incomplete.';
      }
      return la_parsedData;
   }
   catch (err) {
      debugLog(err);
      return -1;
   }
}  //END: parseSensorData


/**************************************************************************
Description:
  Group Vehicle data, aggregated base on day

INPUTS:
  pia_vehicleData => Array of o_vehicle[{ heading: "direction", intervalBy: int, intervalGroups: [o_vehicleGroup] }]
  pis_heading => [optional] heading direction to filter grouping on
OUTPUT:
  la_groupedVehicleData => Array of o_vehicleGroup objects
**************************************************************************/
vcounter.vehiclesGroupByDay = function(pia_vehicleData, pis_heading) {
  var la_groupedVehicleData = [];

  // Scan through vehicle data array
  for (var i = 0; i<pia_vehicleData.length; i++) {    trafficGroup.groupData
    if (pia_vehicleData[i].day !== undefined) {    // Check pia_vehicleData[i].day to see vehicle object meets mandatory requirement

      if (pis_heading !== undefined) {    // Filter on heading direction if specified
        if (pia_vehicleData[i].heading === pis_heading) {    // Action only on matching heading direction
          if (la_groupedVehicleData[pia_vehicleData[i].day - 1] !== undefined) {    // Check if day group is initialized
            // Push vehicle object into vehicleGroup at the right slot grouping
            la_groupedVehicleData[pia_vehicleData[i].day - 1].vehicles.push(pia_vehicleData[i]);
          }
          else {  // initialize day group
            // Create o_vechicleGroup
            var lo_vehicleGroup = new vcounter.o_vehicleGroup(pia_vehicleData[i].day); // Use day value as group ID
            // push vehicleGroup into returned array, at the location where the day is, minus 1
            la_groupedVehicleData[pia_vehicleData[i].day - 1] = lo_vehicleGroup;
            la_groupedVehicleData[pia_vehicleData[i].day - 1].vehicles.push(pia_vehicleData[i]);
          }
        }
      }
      else {
        if (la_groupedVehicleData[pia_vehicleData[i].day - 1] !== undefined) {    // Check if day group is initialized
          // Push vehicle object into vehicleGroup at the right slot grouping
          la_groupedVehicleData[pia_vehicleData[i].day - 1].vehicles.push(pia_vehicleData[i]);
        }
        else {  // initialize day group
          // Create o_vechicleGroup
          // push vehicleGroup into returned array, at the location where the day is, minus 1
          la_groupedVehicleData[pia_vehicleData[i].day - 1] = lo_vehicleGroup;
          la_groupedVehicleData[pia_vehicleData[i].day - 1].vehicles.push(pia_vehicleData[i]);
        }
      }
    }
    else {
      throw "Vehicle object does not have day attribute set." + pia_vehicleData[i];
    }

  }
  return la_groupedVehicleData;
}


/**************************************************************************
Description:
  Group Vehicle data, aggregated base on heading direction

INPUTS:
  pia_vehicleData => Array of o_vehicle objects

OUTPUT:
  la_groupedVehicleData => Array of o_vehicleGroup objects, groupID = heading direction
**************************************************************************/
vcounter.vehiclesGroupByHeading = function(pia_vehicleData) {
  var la_groupedVehicleData = [];
  var la_headings = [];

  // Scan through vehicle data array
  for (var i = 0; i<pia_vehicleData.length; i++) {
    // Check la_headings[] to see if heading is defined
    var ln_headingIndex = -1;
    for (var j=0; j<la_headings.length; j++) {    // Scan la_headings[] to see if heading direction is defined
      if (pia_vehicleData[i].heading === la_headings[j]) {    // If pia_vehicleData[i].heading is defined
        ln_headingIndex = j;
      }
    }

    if (ln_headingIndex < 0) {  // If heading group is not initialized
      // Init new heading direction
      la_headings.push(pia_vehicleData[i].heading);
      ln_headingIndex = la_headings.length - 1;
    }

    // Check to see if la_groupedVehicleData[ln_headingIndex] is initialized with a o_vehicleGroup
    if (la_groupedVehicleData[ln_headingIndex] === undefined) {  // If group is NOT initialized
      var lo_vehicleGroup = new vcounter.o_vehicleGroup(pia_vehicleData[i].heading);    // init group ID = heading direction
      la_groupedVehicleData[ln_headingIndex] = lo_vehicleGroup;
      la_groupedVehicleData[ln_headingIndex].vehicles.push(pia_vehicleData[i]);
    }
    else {
      la_groupedVehicleData[ln_headingIndex].vehicles.push(pia_vehicleData[i]);
    }
  }

  return la_groupedVehicleData;
}


/**************************************************************************
Description:
  Calculates the vehicle separation distance between a vehicle and the one
  which occurred before it.

  Sets a new property:
    o_vehicle.vehicleDist

  o_vehicle.vehicleDist  => separation distance in meters

  Note:  The first vehicle in the array will assume a vehicleDist = 0

  Uses the front and rear axel timestamp and estimated speed property of both vehicles.
  Applies the calculation base on the following formula
  d = v t

  where,
  d  = vehicleDist
  v  = estimated speed of previous vehicle
  t  = elapsed time between the vehicles, using time difference between
       first vehicle rearAxelTime and second vehicle frontAxelTime


INPUTS:
  pioa_vehicleData => Array of o_vehicle objects

OUTPUT:
  pioa_vehicleData => Array of o_vehicle objects with new vehicleDist property
**************************************************************************/
vcounter.vehiclesCalcSeparation = function(pia_vehicleData) {
   // Init first vehicle with a vehicleDist = 0
   // console.log("Inside vehiclesCalcSeparation:...");
   // console.log(pia_vehicleData);
   la_vehicleData = pia_vehicleData;
   la_vehicleData[0].vehicleDist = 0;

   // start at second vehicle
   for (var i=1; i<la_vehicleData.length; i++) {

      // Apply calculation d = vt
      var deltaT = (la_vehicleData[i].frontAxelTime - la_vehicleData[i - 1].rearAxelTime) / vcounter.DEF_MS_IN_SECOND;

      if (la_vehicleData[i].frontAxelTime < la_vehicleData[i - 1].rearAxelTime) {  // Check if the day has changed IF so re-calculate deltaT differently
         // console.log("New day incident found.");
         // console.log("currfrontAxel: " + la_vehicleData[i].frontAxelTime + "  -  " + "prevRearAxel: " + la_vehicleData[i - 1].rearAxelTime);
         deltaT = (la_vehicleData[i].frontAxelTime + (vcounter.DEF_MS_IN_DAY - la_vehicleData[i - 1].rearAxelTime) ) / vcounter.DEF_MS_IN_SECOND;
         // console.log("New day deltaT: " + deltaT);
      }

      // ln_prevVehicleDist = this.vehicles[i - 1].get_estSpeed() * deltaT;
      // ln_currVehicleDist = this.vehicles[i].get_estSpeed() * deltaT;
      la_vehicleData[i].vehicleDist = deltaT * la_vehicleData[i - 1].get_estSpeed();

      // console.log("deltaT: " + deltaT + " - " + "vehicleDist: " + la_vehicleData[i].vehicleDist);
   }

  return la_vehicleData;
}


/***************************************************************************************
DESCRIPTION: vehiclesGroupByInterval
      Groups vehicle data by heading direction, then groups by pin_interval time segements

INPUTS:
      pia_vehicleData   => Array of o_vehicle
      pin_interval      => interval in minutes to group the data in
OUTPUTS:
      [{ heading: "direction", intervalBy: int, intervalGroups: [o_vehicleGroup] }]   =>
                        Array of objects containing properties:
                        - heading
                        - intervalBy
                        - intervalGroups

                        Where "heading" is direction of traffic.
                        Where "intervalBy" is interval division in milliseconds
                        Where "intervalGroups" is an array of o_vehicleGroup and
                        each o_vehicleGroup is an interval grouping of o_vehicle

                        o_vehicleGroup.groupID = interval segment ID in milliseconds

                           e.g.

                           results = [
                              {
                                 heading: "north",
                                 intervalBy: 3600000,   (60 minutes)
                                 intervalGroups: [o_vehicleGroup]
                              }
                           ]
***************************************************************************************/
vcounter.vehiclesGroupByInterval = function (pia_vehicleData, pin_interval) {
   // Call Group by heading first to sort data by heading direction
   var lo_traffGroupedByHeading = vcounter.vehiclesGroupByHeading(pia_vehicleData);

   // Group by intervals
   var ln_intervalInMS = pin_interval * vcounter.DEF_MS_IN_MINUTE;
   var results = [];

   for (var i=0; i<lo_traffGroupedByHeading.length; i++) {
      var intervalGroups = [vcounter.o_vehicleGroup(0)]; // init array

      for (var j=0; j<lo_traffGroupedByHeading[i].vehicles.length; j++) {

         var idx_group = Math.floor(lo_traffGroupedByHeading[i].vehicles[j].get_frontAxelRealTime().getTime() / ln_intervalInMS);

         if (intervalGroups[idx_group] !== undefined) {  // IF group interval already exists
            intervalGroups[idx_group].vehicles.push(lo_traffGroupedByHeading[i].vehicles[j]);  // add vehicle to group interval
         }
         else {   // create the new index for the new group interval
            var lo_vehicleGroup = new vcounter.o_vehicleGroup(idx_group * ln_intervalInMS);
            intervalGroups[idx_group] = lo_vehicleGroup;
            intervalGroups[idx_group].vehicles.push(lo_traffGroupedByHeading[i].vehicles[j]);
         }
      }

      // add to results
      var groupResults = {
         heading: lo_traffGroupedByHeading[i].groupID,
         intervalBy: ln_intervalInMS,
         intervalGroups: intervalGroups
      };
      results.push(groupResults);
   }
   return results;

}
