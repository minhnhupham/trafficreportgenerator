 'use strict';

/* Directivess */
var vLoggerDirectives = angular.module('vehicleLoggerDirectives', []);

vLoggerDirectives.directive('vldUploadFile', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      pio_fileHandler: "=fileHandler",
      pio_fileData: "=fileData"
    },    
    templateUrl: 'directives/vldUploadFile.html',
    link: function (scope, elements, attrs) {
            // var elem_input = elements.children().children();
            var elem_input = elements.find("input");
            elem_input.bind('change', function () {
              var f = elem_input[0].files[0];
              var r = new FileReader();
              r.onloadend = function(e) {
                var fileData = e.target.result;
                scope.$apply(function(){
                  scope.pio_fileData = fileData;  
                });                                                
              }
              if(f !== undefined) {
                r.readAsText(f);                              
                scope.$apply(function() {
                  scope.pio_fileHandler = f;
                });      
              }
            });

          }
    };
});


vLoggerDirectives.directive('vldTrafficReport', function () {
   return {
      restrict: 'E',
      replace: true,
      // transclude: true,
      scope: {
         trafficReportData: '=trafficReportData',   // Expects [{ heading: "direction", intervalBy: int, intervalGroups: [o_vehicleGroup] }]
         chartOptions: '=',
         groupBy: '='
      },
      templateUrl: 'directives/vldTraffReport.html',
      link: function (scope, elems, attrs) {
         var data = new google.visualization.DataTable();

         // Generate Google Chart Data format from trafficReportData               
         data.addColumn('timeofday', 'Time');
         for (var headingIdx=0; headingIdx<scope.trafficReportData.length; headingIdx++) {         
            var headingGroups = scope.trafficReportData[headingIdx];
            data.addColumn('number', headingGroups.heading + 'Bound');   
         }            
                  
         var dataTables = [];
         for (var headingIdx=0; headingIdx<scope.trafficReportData.length; headingIdx++) {            
            // Create data tables            
            var myDataTable = new google.visualization.DataTable();
            myDataTable.addColumn('date', 'Time');
            myDataTable.addColumn('number', scope.trafficReportData[headingIdx].heading + 'bound');
            
            var intervalGroups = scope.trafficReportData[headingIdx].intervalGroups;            
            for (var interval=0; interval<intervalGroups.length; interval++) {
               var lo_vehicleGroup = intervalGroups[interval];
               if (lo_vehicleGroup !== undefined) {
                  // console.log("groupID: " + lo_vehicleGroup.groupID + " - Count: " + lo_vehicleGroup.get_vehiclesCount());
                  var myDate = new Date(lo_vehicleGroup.groupID);
                  myDataTable.addRow([{v: myDate}, lo_vehicleGroup.get_vehiclesCount()]);
               }            
            }
            if (myDataTable.getNumberOfRows() > 0) {  //If myDataTable has more then 0 data rows               
               dataTables.push(myDataTable);
            }
         }
         // console.log("dataTables:...");
         // console.log(dataTables);         
         var options = {
            title: 'Traffic Survey Report Summary',
            legend: {position: 'top'},
            hAxis: {
               title: 'Time',
               format: '\'Day\' dd \'-\' hh:mm aa',
               textPosition: 'out',
               //viewWindow: {
                  //min: [7, 30, 0],
                  //max: [17, 30, 0]
               //},
               gridlines: {
                  count: -1,
                  units: {
                     days: {format: ['\'Day\' dd']},
                     hours: {format: ['hh:mm aa', 'hh:mm a']},
                  }                             
               }
            },            
            vAxis: {
               title: 'Vehicle Count'
            }
         };

         var chart = new google.visualization.ColumnChart(
            document.getElementById('chartID'));

         //chart.draw(data, options);
         
         // dataTables of both heading directions
         var joinedDT = google.visualization.data.join(dataTables[0], dataTables[1], 'full', [[0,0]], [1], [1]);
         
         var formatter = new google.visualization.DateFormat({pattern: '\'Day\' dd \'-\' hh:mm aa', timeZone: +0});
         // Reformat the Time column to display readable times.
         formatter.format(joinedDT, 0);         
         
         chart.draw(joinedDT, options);
         /*
         scope.$watch('groupBy', function() {            
            console.log("groupBy changed: " + scope.groupBy);
            chart.draw(joinedDT, options);
         });
         */
         
         /* // This uses the vis.js library instead of Google Charts
         var container = document.getElementById('chartID');
         var items = [];
         var groups = new vis.DataSet();
         for (var dataIdx=0; dataIdx < scope.trafficReportData.length; dataIdx++) {
            var group = scope.trafficReportData[dataIdx].heading;
            groups.add({id: group, content: group + "bound"});
            console.log("intervalGroups:...");
            console.log(scope.trafficReportData[dataIdx].intervalGroups);
            for (var vehicleGroupIdx=0; vehicleGroupIdx < scope.trafficReportData[dataIdx].intervalGroups.length; vehicleGroupIdx++) {
               if (scope.trafficReportData[dataIdx].intervalGroups[vehicleGroupIdx] !== undefined) {
                  console.log(scope.trafficReportData[dataIdx].intervalGroups[vehicleGroupIdx]);
                  var intervalId = scope.trafficReportData[dataIdx].intervalGroups[vehicleGroupIdx].groupID;               
                  var vehicleCount = scope.trafficReportData[dataIdx].intervalGroups[vehicleGroupIdx].get_vehiclesCount();

                  var item = {
                     x: intervalId,
                     y: vehicleCount,
                     group: group,
                     label: {content: vehicleCount, xOffset: -10, yOffset: -5}
                  };
                  items.push(item);                  
               }

            }
                           
         }

         var dataset = new vis.DataSet(items);
         var options = {
            style:'bar',
            stack:false,
            barChart: {width:50, align:'right', sideBySide:true}, // align: left, center, right
            drawPoints: {enabled: true, style: 'circle', size: 3},
            //dataAxis: {
               //  icons:true
            //},
            legend: {enabled: true, left: {position: 'top-right'} },
            orientation:'bottom'
            //start: '2014-06-10',
            //end: '2014-06-18'
         };
         var graph2d = new vis.Graph2d(container, items, groups, options);
         */

      }
   }
});
