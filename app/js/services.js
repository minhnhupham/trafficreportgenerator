 'use strict';

/* Services */
var vLoggerService = angular.module('vehicleLoggerServices', []);

vLoggerService.service('vlsTrafficDataServ',['$log', function($log) {
   var lo_vlsTrafficDataServ = this;
   lo_vlsTrafficDataServ.errorStatus = {
      errCode: 0,
      errMsg: ''
   };

   lo_vlsTrafficDataServ.fileHandlerData = {name: ""};
   lo_vlsTrafficDataServ.rawData = "";
   lo_vlsTrafficDataServ.groupBy = 60;

   // Init VCOUNTER
   var vlsCounter = vcounter;

   /*******************************************************************
    NAME: getRepByInterval
    DESCRIPTION:
                 This is defined in vcounter.js
    INPUTS:
    OUTPUTS:
   ********************************************************************/
   lo_vlsTrafficDataServ.getRepByInterval = function (pis_rawData, pis_interval) {
      try {
         if (pis_rawData.length <= 0) {
            lo_vlsTrafficDataServ.errorStatus.errCode = 1;
            lo_vlsTrafficDataServ.errorStatus.errMsg = "No Data Loaded.";
            throw "No data loaded.";
         }
         else {
            var results = [];
            var ln_interval = pis_interval;  // interval in minutes
            // var la_parsedData = vcounter.parseSensorData(pis_rawData);
            var la_parsedData = vlsCounter.parseSensorData(pis_rawData);
            results = vlsCounter.vehiclesGroupByInterval(la_parsedData, ln_interval);
            /*
            results = [
               {
                  heading: "north",
                  intervalBy: 3600000,
                  intervalGroups: [o_vehicleGroup]
               }
            ]
            */
            return results;
         }

      }
      catch(err) {
         $log.error("ERROR in: getRepByInterval");
         $log.error(err);
      }
   };


}]);
