'use strict';

/*We need to manually start angular as we need to
wait for the google charting libs to be ready*/  
google.setOnLoadCallback(function () {  
    angular.bootstrap(document.body, ['vehicleLogger']);
});

// google.charts.load('current', {'packages':['bar']});

// Declare app level module which depends on views, and components
var vLogger = angular.module('vehicleLogger', [
  'ngRoute',
  'ngResource',
  'vehicleLoggerControllers',
  'vehicleLoggerDirectives',
  'vehicleLoggerServices'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'pages/vehiclelogger.html',
      controller: 'vLoggerCtrl'
    })
    .when('/vehiclelogger', {
      templateUrl: 'pages/vehiclelogger.html',
      controller: 'vLoggerCtrl'
    })
    .when('/trafficreport', {
      templateUrl: 'pages/trafficreport.html',
      controller: 'trafficRepCtrl'
    })
    .otherwise({redirectTo: '/'});
}]);
