'use strict';

/* Controllers */

var vLoggerControllers = angular.module('vehicleLoggerControllers', []);

vLoggerControllers.controller('vLoggerCtrl', ['$scope', '$log', 'vlsTrafficDataServ',
  function($scope, $log, vlsTrafficDataServ) {
    $scope.lo_vlsTrafficDataServ = vlsTrafficDataServ;

    $scope.getFilename = function () {
      return $scope.lo_vlsTrafficDataServ.fileHandlerData.name;
    };

      // pass raw sensor data into trafficDataServ
      $scope.processFile = function (pio_trafficDataServ) {
      try {
          pio_trafficDataServ.errorStatus = {
             errCode: 0,
             errMsg: ''
          };

          if (pio_trafficDataServ !== undefined && pio_trafficDataServ.fileHandlerData !== undefined ) {
              var la_parsedData = vcounter.parseSensorData(pio_trafficDataServ.rawData);

              if (la_parsedData === -1) {
                 throw "Invalid input file.";
              }
              $scope.vehiclesData = la_parsedData;
          }
          else {
              throw "No file selected.";
          }
      }
      catch (err) {
          vlsTrafficDataServ.errorStatus = {
             errCode: 1,
             errMsg: err
          };
          $log.error(err);
      }
    };

  }]);


vLoggerControllers.controller('trafficRepCtrl', ['$scope', '$log', 'vlsTrafficDataServ',
   function($scope, $log, vlsTrafficDataServ) {
      $scope.trafficDataServ = vlsTrafficDataServ;

      // $scope.trafficDataServ.groupBy = 60;
      $scope.traffRepByInterval = vlsTrafficDataServ.getRepByInterval($scope.trafficDataServ.rawData, $scope.trafficDataServ.groupBy);


      $scope.$watch('trafficDataServ.groupBy', function() {
         $log.log('groupBy has changed to: ' + $scope.trafficDataServ.groupBy);
         $scope.traffRepByInterval = vlsTrafficDataServ.getRepByInterval($scope.trafficDataServ.rawData, $scope.trafficDataServ.groupBy);
      });

      $scope.chartData = "";
   }]);
