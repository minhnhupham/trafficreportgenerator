describe('vehicleLogger Controllers', function() {
   beforeEach(module('vehicleLogger'));

   describe('vLoggerCtrl', function(){
      var scope;
      var ctrl = {};
      var vlsTrafficDataServ = {};
      var filename = "testDataFile.txt";

      beforeEach(inject(function($controller, $rootScope, _vlsTrafficDataServ_) {  // Initialize the controller, mock scope and vlsTrafficDataService
         scope = $rootScope.$new();
         vlsTrafficDataServ = _vlsTrafficDataServ_;
         ctrl.vLoggerCtrl = $controller('vLoggerCtrl', {$scope:scope});
         vlsTrafficDataServ.fileHandlerData.name = filename;
         vlsTrafficDataServ.rawData = [
            "A98186",
            "A98333",
            "A499718",
            "A499886",
            "A638379",
            "B638382",
            "A638520",
            "B638523",
            "A1016488",
            "A1016648",
            "A1058535",
            "B1058538",
            "A1058659",
            "",
            "B1058662",
            "A1201386",
            "B1201389",
            "A1201539",
            "B1201542",
            "A1624044",
            "B1624047",
            "A1624188",
            "B1624191",
            "A1782481",
            "B1782484",
            "A1782660",
            "B1782664",
            "A2146213",
            "A2146349",
            "",
            "A2147213",
            "A2147349",
            "",
            "A86380213",
            "A86380349",
            "A86385213",
            "A86385349",
            "A1186",
            "A1333",
            "A98186",
            "A98333",
            "A100333",
            "A100480"
         ].join("\n");
      }));

      it('...should be defined', function() {
         expect(ctrl.vLoggerCtrl).toBeDefined();
      });

      it ('...should have a vlsTrafficDataServ service', function() {
         expect(scope.lo_vlsTrafficDataServ).toBeDefined();
      });

      it('...vlsTrafficDataServ should have filename property', function() {
         expect(scope.lo_vlsTrafficDataServ.fileHandlerData.name).toEqual("testDataFile.txt");
      });

      it('...vlsTrafficDataServ should have raw sensor data', function() {
         expect(scope.lo_vlsTrafficDataServ.rawData).toBeDefined();
         expect(vlsTrafficDataServ.rawData).not.toBe(null || '');
      });

   });

   describe('trafficRepCtrl', function(){
      var scope;
      var ctrl = {};

      beforeEach(inject(function($controller, $rootScope) {
         scope = $rootScope.$new();
         ctrl.trafficRepCtrl = $controller('trafficRepCtrl', {$scope:scope});
      }));

      it('trafficRepCtrl...should be defined', function() {
         expect(ctrl.trafficRepCtrl).toBeDefined();
      });

   });
});
